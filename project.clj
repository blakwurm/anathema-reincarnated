(defproject anathemaclj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-kotlin "0.0.2"]]
  :kotlin-source-path "src/kt"
  :kotlin-compiler-version "1.0.4"
  :repositories [["in-sidefx" "https://dl.bintray.com/in-sidefx/maven"]]
  :dependencies [[org.clojure/clojure "1.9.0-alpha13"]
                 [lytek "0.8.1-SNAPSHOT"]
                 [com.rpl/specter "0.13.1"]
                 [org.jetbrains.kotlin/kotlin-runtime "1.0.4"]
                 [draconic-fx "0.3.0-SNAPSHOT"]
                 ;; https://mvnrepository.com/artifact/org.controlsfx/controlsfx
                 [org.controlsfx/controlsfx "8.40.10"]
                 [insidefx.undecorator/undecorator "0.1.0"]
                 ]
  :profiles {:repl {:dependencies [[org.jetbrains.kotlin/kotlin-compiler "1.0.4"]]}}
  :prep-tasks ["kotlin"]
  ;:aot :all
  )
