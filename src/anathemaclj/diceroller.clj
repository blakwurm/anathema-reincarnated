(ns anathemaclj.diceroller
  (:require
    [clojure.java.io :as io]
    [draconic.fx.ml :as dracml :refer :all]
    [draconic.fx :as dracfx :refer :all]
    [draconic.fx.assignment :as dracas :refer :all]
    [com.rpl.specter :refer :all]
    [anathemaclj.undec :as undec]))

(declare diceview)

(def ten-range (range 1 11))

(defn roll-d-10 [] (+ 1 (int (rand 10))))

(def min-for-success 7)

(defn count-success [running-results success? double?]
  (transform [(keypath :result)]
             (fn [thingy] (if success? (+ (if double? 2 1) thingy) thingy)) running-results))
(defn count-rerolls [running-results reroll?]
  (transform [keypath :rerolls-left] (fn [thingy] (if reroll? (inc thingy) thingy)) running-results))
(defn put-successes [running-results success? result]
  (transform [(keypath :success-rolls)] (fn [thingy] (if success? (conj thingy result) thingy)) running-results))
(defn put-rerolls [running-results reroll? result]
  (transform [(keypath :rerolled)] (fn [thingy] (if reroll? (conj thingy result) thingy)) running-results))

(defn get-x-d-10 [x]
  (into [] (map (fn [numbah] (roll-d-10)) (range 0 x))))

(defn parse-roll-results
  ([roll-vec reroll-numbs double-numbs]
   (parse-roll-results roll-vec reroll-numbs double-numbs
                       {:result        0
                        :rerolls-left  0
                        :success-rolls []
                        :rerolled      []
                        :all-rolls     []}))
  ([roll-vec reroll-numbs double-numbs recurs-results]
   (let [recurs-results-with-these-rolls (transform [(keypath :all-rolls)] #(conj % roll-vec) recurs-results)
         {:keys [rerolls-left] :as result-map} (reduce (fn [running-results roll-result]
                                                         (let [success? (>= roll-result min-for-success)
                                                               double? (contains? (into #{} double-numbs) roll-result)
                                                               reroll? (contains? (into #{} reroll-numbs) roll-result)
                                                               counted-result (-> running-results
                                                                                  (count-success success? double?)
                                                                                  (count-rerolls reroll?)
                                                                                  (put-successes success? roll-result)
                                                                                  (put-rerolls reroll? roll-result))]
                                                           counted-result))
                                                       recurs-results-with-these-rolls
                                                       roll-vec)]
     (cond
       (= 10 (count reroll-numbs)) [[:reroll-numbs "Rerolling all results will cause an infinite loop."]]
       (> rerolls-left 0) (parse-roll-results (get-x-d-10 rerolls-left)
                                              reroll-numbs
                                              double-numbs
                                              (setval [(keypath :rerolls-left)] 0 result-map))
       :else result-map))))

(defn roll-dice
  [numb-dice reroll-numbs double-numbs]
  (parse-roll-results
    (get-x-d-10 numb-dice)
    reroll-numbs
    double-numbs))

(defn get-selected-numbah-boxen [map-o-things teh-name]
  (let [vecco (reduce (fn [selected-so-far numbah]
                        (let [teh-key (str teh-name numbah)
                              checkbox (get map-o-things teh-key)]
                          (if (.isSelected checkbox)
                            (conj selected-so-far numbah)
                            selected-so-far)))
                      [] ten-range)]
    vecco))

(defn make-text-for-display [full-results]
  (cond
    (map? full-results) (let [{:keys [result rerolls-left success-rolls rerolled all-rolls]} full-results]
                          (str "\n" result " Successes! Counted Results: " success-rolls "\n"
                               "              Results Rerolled: " rerolled "\n"
                               "              All results, in roll order: " all-rolls "\n"))
    (vector? full-results) (str (reduce (fn [message-so-far [error-key error-message]] (str message-so-far "\n" error-message)) "Sorry, couldn't roll. Reason?" full-results) "\n")))

(dracml/defcontroller dice-roller-control
  "Sets the stuff what makes the buttons roll the things"
  [mappo rollreadout rollButton incRollAmount decRollAmount rollAmountField]
  (.setOnAction rollButton
                (event-handler [e] (do
                                     (let [numbah-box-readout (read-string (.getText rollAmountField))
                                           roll-times (if (not (integer? numbah-box-readout)) 3 numbah-box-readout)
                                           reroll-numbs (get-selected-numbah-boxen mappo "reroll")
                                           double-numbs (get-selected-numbah-boxen mappo "double")
                                           roll-results (roll-dice roll-times reroll-numbs double-numbs)
                                           readout-id "#rollreadout"
                                           display-text (make-text-for-display roll-results)]
                                       (.appendText rollreadout display-text)))))
  (.setOnAction incRollAmount
                (event-handler [e] (do
                                     (let [current-readout (read-string (.getText rollAmountField))
                                           current-dice-number (if (integer? current-readout) current-readout 0)]
                                       (.setText rollAmountField (str (inc current-dice-number)))))))
  (.setOnAction decRollAmount
                (event-handler [e] (do
                                     (let [current-readout (read-string (.getText rollAmountField))
                                           current-dice-number (if (integer? current-readout) current-readout 0)]
                                       (.setText rollAmountField (str (dec current-dice-number))))))))

(defn launch-dice-roller [] (dracml/launch-fxml-window ["resources/ui/diceroller.fxml"]
                                                       #'dice-roller-control
                                                       (undec/style-undecorated "Dice Roller")))

(comment
  )