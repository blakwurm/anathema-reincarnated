(ns anathemaclj.controllers
  (:require [draconic.fx.ml :refer :all]
            [draconic.fx :as fx :refer :all])
  (:import (javafx.scene.control TitledPane)))

(defcontroller expando-and-retracto
  "Expands and retracts all the titled panes in a node called sheetView"
  [el-mappo sheetView expandAllButton collapseAllButton]
  (.setOnAction expandAllButton
                (event-handler [e]
                  (doseq [thingy (.getChildren sheetView)]
                    (.setExpanded thingy true))))
  (.setOnAction collapseAllButton
                (event-handler [e]
                  (doseq [thingy (.getChildren sheetView)]
                    (.setExpanded thingy false)))))



(defcontroller solar-sheet-controller
  "Controlls the solar character sheet. For better or for worse."
  [el-mappo overview]
  (expando-and-retracto el-mappo)

  )
(comment
  (defn control-character-sheet-with-map
    [{:strs [abilityFlow
             overview
             expandAllButton
             validationLabel
             charSheetInsert]
      :as   mappo}]
    (let [ability-widgets-map (make-ability-widget-map abilityFlow)]
      (def last-launched-char-sheet-map mappo)
      (ui-set! :on-action expandAllButton
               (fn [e]
                 (let [charSheetInsert (->> overview
                                            (.getContent)
                                            (ui-get :children)
                                            (first))]
                   (into [] (map #(do (println %)
                                      (.setExpanded % true)
                                      (println (.isExpanded %)))
                                 (ui-get :children charSheetInsert))))))
      (ui-set! :on-mouse-click validationLabel
               (fn [e]
                 (into [] (map (fn [ability]
                                 (println (get-current-ability-info ability ability-widgets-map)))
                               lytek.character.elements/abilities))))
      (map (fn [ability]
             (let [rating-node (get ability-widgets-map (str ability "-scoreRating"))]
               (ui-set! :on-mouse-click rating-node
                        (fn [e] (println (get-current-ability-info ability ability-widgets-map))))))
           lytek.character.elements/abilities)
      (def last-launched-ability-widget-map ability-widgets-map))))