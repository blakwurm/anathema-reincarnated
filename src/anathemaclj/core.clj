(ns anathemaclj.core
  (:import (com.blakwurm.anathema Sampler)))

(defn foo
  "I don't do a whole lot."
  [x, joiner]
  (let [smplr (new Sampler x)]
    (do (.checkMessage smplr)
        (.doAThing smplr (fn [thing1 thing2 thing3]
                           (str thing1 " " joiner " " thing2 ", and then " thing3))))))
"\ui\diceroller.fxml"