(ns ^:no-doc anathemaclj.fx-launcher
  (:require [anathemaclj.fx-util :refer :all]
            [lytek.macros :as lymac])
  (:import (javafx.scene.input KeyCode)
           (javafx.stage Modality StageStyle)
           (javafx.fxml FXMLLoader)
           (insidefx.undecorator UndecoratorScene)
           (com.blakwurm.anathema.fx UndecHelper anathaFX)))


(defn launch-window
  [refresh-fn stage-set-fn & {:keys [title maximized stylesheet]
                              :or   {title (str "Sandbox")}}]

  (do
    (javafx.embed.swing.JFXPanel.)
    (javafx.application.Platform/setImplicitExit false)
    (lymac/dbg (make-window-with-map refresh-fn stage-set-fn))))


  ;(run<!!
  ;  (let [init-stage (fx/stage)
  ;        init-node (refresh-fn)
  ;        [scene, stage] (.undecorate (UndecHelper/funs)
  ;                                    init-node)
  ;        ]
  ;    (pset! scene
  ;           {:on-key-pressed
  ;            (fn do-sandbox-refresh [e]
  ;              (when (= KeyCode/F5 (.getCode e))
  ;                (.refreshUndec (UndecHelper/funs)
  ;                               (refresh-fn)
  ;                               scene,
  ;                               stage)))})
  ;    (.setScene stage scene)
  ;    (.initModality stage Modality/NONE)
  ;    (pset! stage {:title title})
  ;    (when maximized (.setMaximized stage true))
  ;    (.show stage)
  ;    stage))
