(ns anathemaclj.solar-sheet
  (:require
    [clojure.java.io :as io]
    [draconic.fx :as dracfx :refer :all]
    [draconic.fx.ml :as dracml :refer :all]
    [draconic.fx.assignment :as dracas :refer :all]
    [anathemaclj.fx-launcher :as launcher]
    [anathemaclj.fx-util :refer :all]
    [com.rpl.specter :refer :all]
    [lytek.macros :as lymac]
    [clojure.string :as str]
    [lytek.character.elements :as lyelem]
    [anathemaclj.undec :as undec])
  (:import (javafx.fxml FXMLLoader)
           (insidefx.undecorator UndecoratorScene)
           (javafx.scene.layout FlowPane)))


(defn keywords-as-symbols [seq-of-keywords]
  (into '[] (map (fn [a-keyword]
                   (-> a-keyword
                       (key-to-str)
                       (symbol)))

                 seq-of-keywords)))

(def character-sheet-fields [:overview :charSheetInsert :removeMeritButton :limitRating
                             :expandAllButton :collapseAllButton])

(defn keyword-to-string [keyverd]
  (str/replace (str keyverd) ":" ""))

(defn cull-null-entries [associative-thingy]
  (into {} (select [ALL (fn [[k v]] (not (nil? v)))] associative-thingy)))

(defn get-current-ability-info [ability widgets-map]
  (let [ability-str (keyword-to-string ability)
        score-key (str ability-str "-scoreRating")
        status-key (str ability-str "-statusCheck")
        score-number (int (ui-get :numeric-value (get widgets-map score-key)))
        status-widget (get widgets-map status-key)
        ability-status (cond (ui-get :indeterminate status-widget) :caste
                             (ui-get :selected status-widget) :favored
                             :else :unselected)]
    [ability score-number ability-status]))

(defn make-ability-spcific-thingy
  "Loads the ability rank widget, and then makes it spcific to the individual ability"
  [ability]
  (let [[nodey mappo] (load-node-with-id-map "ui/abilityBit.fxml")
        new-mappo (cull-null-entries
                    (map (fn [[k v]]
                           (let [new-name (str ability "-" k)]
                             [new-name (lymac/try-nil
                                         (doto v (.setId new-name)))]))
                         mappo))]
    (.setId nodey (str ability "-widget"))
    (ui-set! :text (get new-mappo (str ability "-nameLabel")) (str/capitalize ability))

    [nodey
     new-mappo]))



(defn make-single-ability-widget-entry [[assembled-widgets
                                         built-map-o-ability-widgets]
                                        ability]
  (let [[widget-node mappo-things] (make-ability-spcific-thingy ability)]
    [(conj assembled-widgets widget-node)
     (into built-map-o-ability-widgets mappo-things)]))

(defn make-ability-widget-map
 [flow-thing]
   (let [[ability-widgets ability-widgets-map] (reduce make-single-ability-widget-entry [[] {}]
                                                       (sort (map keyword-to-string lyelem/abilities)))]
     (.. flow-thing
         (getChildren)
         (addAll ability-widgets))
     ability-widgets-map))

(defn make-ability-widgets
  []
  (let [[ability-widgets ability-widgets-map] (reduce make-single-ability-widget-entry [[] {}]
                                                      (sort (map keyword-to-string lyelem/abilities)))
        flowy (new javafx.scene.layout.FlowPane)]
    (draconic.fx.assignment/try-set! flowy :children ability-widgets)
    (set-id! flowy "abilityFlow")
    (println "flowy is " flowy)
    [flowy (into ability-widgets-map {"abilityFlow" flowy})]))



(defn launch-char-sheet [] (launch-fxml-window
                          ["resources/ui/solarcharactermanager.fxml"
                           ["overview"
                            "resources/ui/solarcharsheet_replacement.fxml"]
                           ["healthPane"
                            "resources/ui/healthtrack.fxml"]
                           ["abilityBox"
                            make-ability-widgets]]
                          anathemaclj.controllers/solar-sheet-controller
                          (undec/style-undecorated "Solar Manager")))




