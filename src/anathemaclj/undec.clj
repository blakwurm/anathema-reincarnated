(ns anathemaclj.undec
  (:require [draconic.fx :refer :all])
  (:import (com.blakwurm.anathema.fx UndecHelper)
           (javafx.stage StageStyle Stage)
           (insidefx.undecorator UndecoratorScene)
           (javafx.scene.paint Color)))

(defn style-undecorated [the-title]
  (fn [the-node]
   (run-now (let [stylepath "ui/mod-indy.css"
                  undecfxmlpath "/ui/windowDec.fxml"
                  ___--- (do (set! UndecoratorScene/STAGEDECORATION undecfxmlpath)
                             (set! UndecoratorScene/STYLESHEET stylepath))
                  the-stage (new Stage StageStyle/UTILITY)
                  the-scene (new UndecoratorScene the-stage the-node)]
              (.add (.getStylesheets the-scene) stylepath)
              (.setFill the-scene Color/TRANSPARENT)
              (.setScene the-stage the-scene)
              (.setTitle the-stage the-title)
              the-stage))
    ))