(ns anathemaclj.fx-util
  (:require [clojure.string :as str]
            [lytek.macros :refer :all]
            [clojure.java.io :as io]
            [lytek.macros :as lymac])
  (:import (javafx.stage Stage)
           (com.blakwurm.anathema.fx anathaFX)
           (javafx.scene.control CheckBox)
           (org.controlsfx.control Rating)))

(javafx.embed.swing.JFXPanel.)
(javafx.application.Platform/setImplicitExit false)

(defn key-to-str [a-keyword]
  (str/replace (str a-keyword) ":" ""))

(defn add-action-listener [node listener-fn]
  (.setOnAction node (anathaFX/toEventHandler listener-fn)))
(defn add-mouse-click-listener [node listener-fn]
  (.setOnMouseClicked node (anathaFX/toMouseEventHandler listener-fn)))


(defn lookup-all [container-of-scene-graph seq-of-keys]
  (let [node (if (= (class container-of-scene-graph) Stage)
               (.getScene container-of-scene-graph)
               container-of-scene-graph)]
    (into {:root node} (map (fn [thingy]
                              (let [thingy-string (str "#" (if (keyword? thingy)
                                                             (key-to-str thingy)
                                                             thingy))]
                                [thingy (.lookup node thingy-string)]))

                            seq-of-keys))))

(defn make-window [maker-fn stage-set-fn]
  (anathaFX/makeWindow maker-fn stage-set-fn))

(defn make-window-with-map [maker-fn stage-set-fn]
  (anathaFX/makeWindowAndIDMap maker-fn stage-set-fn))

(defn run-in-fx [to-run-fn]
  (anathaFX/runInFX to-run-fn))

(defmulti-using-map ui-handle-numeric-value "Gets a numeric value from a UI control"
  [get-or-set node new-value]
  [get-or-set (type node)]
  {[:get Rating] (.getRating node)
  [:set Rating] (.setRating node new-value)})

(defmulti-using-map ui-set! "Sets a bit of state in a UI node"
  [what-keyword node thing-to-add]
  what-keyword
  {:text      (.setText node thing-to-add)
   :on-action (add-action-listener node thing-to-add)
   :on-mouse-click (add-mouse-click-listener node thing-to-add)
   :numeric-value (ui-handle-numeric-value :set node thing-to-add)})

(defmulti-using-map ui-get "Gets a bit of state from a UI node"
  [what-keyword node] what-keyword
  {:text     (.getText node)
   :children (into [] (.getChildrenUnmodifiable node))
   :numeric-value (ui-handle-numeric-value :get node 0)
   :selected (.isSelected node)
   :indeterminate (.isIndeterminate node)})

(defn defaction [node action-fn]
  (ui-set! :on-action node action-fn))

(defn loader-for [loc-string]
  (anathaFX/getLoaderFor (io/resource loc-string)))

(defn load-from [loader]
  (anathaFX/getNodeFromLoader loader))

(defn load-node-with-id-map [loc-string]
  (let [[nodey wacky-map] (anathaFX/getNodeWithNamespaces (io/resource loc-string))]
    [nodey (into {} wacky-map)]))

(defn add-node-to-pane [parent node]
  (anathaFX/glueNodeToParent parent node))

(defn put-fx-bits-into-place
  "Takes strings as arguments. First string is the location of the primary javafx
  document to load, and each subsiquent pair of strings maps to an id string of a parent
  node and the location of the javafx file to put in it, respectively"
  [initial-loc & args]
  (let [[initial-node initial-map] (load-node-with-id-map initial-loc)
        pairs (into [] (map vec (partition 2 args)))
        realized-node-map (reduce (fn [map-so-far [this-id this-loc]]
                                    (let [[this-node this-map] (load-node-with-id-map this-loc)]
                                      (do (add-node-to-pane (get map-so-far this-id) this-node)
                                          (into map-so-far (into {} this-map)))))
                                  (into {} initial-map)
                                  pairs)]
    [realized-node-map initial-node]))


(defn lazy-put-fx-bits-into-place
  [initial-loc & args]
  (fn [] (apply put-fx-bits-into-place initial-loc args)))

(defn lazy-load-load-with-id-map
  [loc-string]
  (fn [] (reverse (load-node-with-id-map loc-string))))