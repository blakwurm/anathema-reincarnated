@file:JvmName("UndecHelper")

package com.blakwurm.anathema.fx

import clojure.lang.PersistentVector
import insidefx.undecorator.UndecoratorScene
import javafx.scene.control.TextArea
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.stage.StageStyle

/**
 * Created by achyt_000 on 11/21/2016.
 */
 fun undecorate(mainNode: Region, utility: Boolean, stylepath: String = "ui/mod-indy.css", decPath: String = "/ui/windowDec.fxml") : clojure.lang.IPersistentVector {
    val stage =  Stage(if (utility) StageStyle.UTILITY else StageStyle.DECORATED)
    //stage.isAlwaysOnTop = true
    UndecoratorScene.STYLESHEET = stylepath
    UndecoratorScene.STAGEDECORATION = decPath
    val scene = UndecoratorScene(stage, mainNode)
    scene.stylesheets.add(stylepath)
    scene.fill = Color.TRANSPARENT
    stage.scene = scene
    return PersistentVector.adopt(arrayOf(scene, stage))
    //return Pair(scene, stage)
}

fun undecorate(mainNode: Region) = undecorate(mainNode = mainNode, utility = true)


fun refreshUndec(mainNode: Region, undecoratorScene: UndecoratorScene, stage: Stage) : Stage {
    val scene = UndecoratorScene(stage, mainNode)
    stage.scene = scene
    return stage
}

fun screwAround() {
    TextArea()
}