@file:JvmName("anathaFX")
package com.blakwurm.anathema.fx

import clojure.lang.IFn
import clojure.lang.IPersistentCollection
import clojure.lang.IPersistentMap
import clojure.lang.IPersistentVector
import javafx.application.Platform
import javafx.event.Event
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.FlowPane
import javafx.scene.layout.Region
import javafx.scene.layout.TilePane
import javafx.stage.Stage
import org.controlsfx.control.Rating
import java.net.URL

/**
 * Created by achyt_000 on 11/23/2016.
 */
fun IFn.toEventHandler() : EventHandler<Event> {

    return EventHandler { this(it) }
}

fun IFn.toMouseEventHandler() : EventHandler<MouseEvent> {
    return EventHandler { this(it) }
}

fun makeWindow(nodeMaker: IFn, stageSetter: IFn): Stage? {
    var theStage: Stage? = null
    Platform.runLater {
        val undecorated = undecorate((nodeMaker() as Region))
        val theScene = undecorated.nth(0) as Scene
        theStage = undecorated.nth(1) as Stage

        theStage?.show()

        stageSetter(theStage)
        println(undecorated)
    }
    println(theStage)
    return theStage
}

internal  fun<T,K> IPersistentCollection.asPair(): Pair<T,K> {
    val transformedCol = clojure.`core$into`.invokeStatic(clojure.lang.PersistentVector.EMPTY, this) as IPersistentVector
    return Pair(transformedCol.nth(0) as T, transformedCol.nth(1) as K)
}



fun makeWindowAndIDMap(nodeMaker: IFn, stageSetter: IFn): Stage? {
    var theStage: Stage? = null
    Platform.runLater {

        val (mapOIDs, node) = (nodeMaker() as IPersistentCollection).asPair<IPersistentMap, Node>()

        val (theScene, aStage) = undecorate((node as Region)).asPair<Scene, Stage>()

        theStage = aStage

        theStage?.show()

        stageSetter(mapOIDs.assoc("theStage",theStage))
    }
    println(theStage)
    return theStage
}

fun runInFX(fn: IFn) : Any? {
    var toReturn: Any? = null
    Platform.runLater {
        toReturn = fn()
    }
    return toReturn
}

fun wrapFnInHandler(fn: IFn): javafx.event.EventHandler<Event> {
    return EventHandler { fn(it) }
}

fun getLoaderFor(location: URL) : FXMLLoader {
    return FXMLLoader(location)
}

fun getNodeFromLoader(loader: FXMLLoader) : Node {
    return loader.load<Node>()
}

fun getNodeWithNamespaces(location: URL) : IPersistentVector {
    val loader = FXMLLoader(location)
    val namespace = loader.namespace
    return clojure.lang.PersistentVector.adopt(listOf(loader.load<Node>(), namespace).toTypedArray())
}

fun glueNodeToParent(parent:ScrollPane, node:Node) : Parent? {
    parent?.content = node
    return parent
}

fun glueNodeToParent(parent:TitledPane, node:Node) : Parent? {
    parent?.content = node
    return parent
}

fun addStuffAsChildren(tilePane: TilePane, listOThings: IPersistentCollection) {
    tilePane.children.addAll(listOThings as List<Node>)
}

private fun type_inference_fun(thing: Rating) {
    thing.rating
    (null as CheckBox).isIndeterminate
    (null as ScrollPane)
}