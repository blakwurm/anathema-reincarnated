# Anathema Reincarnated

A Clojure app for creating, managing, and playing characters in Exalted 3rd Edition

## Usage

Currently, not much. Stay tuned!

## License

Copyright © 2016 Blak Wurmwood Studios

Distributed under the Eclipse Public License version 1.0.
